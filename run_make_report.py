from pathlib import Path
from pylake_utils import h5_to_png
import make_report

if __name__ == '__main__':
    # Example usage:

    h5_path = Path('/home/master/projects/lipid-mixing/')
    png_path = h5_path / 'png_path'
    h5_to_png.save_h5_as_png(
        h5_path, png_path
    )
    make_report.process_path_single_continuous(Path(png_path / '20211028-163318 Marker 1'))
    h5_to_png.save_all_images_in_path_for_viewing(png_path)
