from collections import namedtuple
from pathlib import Path
from typing import Generator
from dataclasses import dataclass

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import widgets
from cv2 import cv2

Rect = namedtuple('Rect', 'x1 y1 x2 y2')


@dataclass
class VesicleAndTetherImg:
    vesicle_rect: Rect
    tether_rect: Rect
    img: np.ndarray
    name: str


class SelectableFigure:
    """
    Display an image along with a title, enable rectangle selection tool, and let the user define a region of interest.
    Once the image figure is closed, the last selection is accessible through the selected_rect field.
    """
    def __init__(self, img: np.ndarray, title: str):
        self.fig, self.ax = plt.subplots()    
        self.title = title
        self.img = img.copy()
        self.selected_rect = None
        self._prepare_figure()

    def __call__(self, eclick, erelease):
        self.ax.clear()
        self._prepare_figure()
        self.ax.plot(
            (eclick.xdata, eclick.xdata, erelease.xdata, erelease.xdata, eclick.xdata),
            (eclick.ydata, erelease.ydata, erelease.ydata, eclick.ydata, eclick.ydata),
            'b')
        self.selected_rect = Rect(
            min(round(eclick.xdata), round(erelease.xdata)),
            min(round(eclick.ydata), round(erelease.ydata)),
            max(round(eclick.xdata), round(erelease.xdata)),
            max(round(eclick.ydata), round(erelease.ydata))
        )

    def _prepare_figure(self):
        plt.title(self.title)
        self.selector = widgets.RectangleSelector(self.ax, self)
        self.ax.imshow(self.img)

    def show(self):
        plt.show()


def get_img_rect(img: np.ndarray, title: str) -> Rect:
    """
    Show an image with a title, and let the user select a region from it.
    Args:
        img: The image to be displayed
        title: The title of the figure

    Returns:
        The Rect of the selected region
    """
    s = SelectableFigure(img[:, :, ::-1] / np.max(img), title)  # We convert BGR to RGB with [:, :, ::-1]
    s.show()  # This line is blocking until the figure is closed by the user
    if s.selected_rect:
        selected_img = img[s.selected_rect.y1:s.selected_rect.y2, s.selected_rect.x1:s.selected_rect.x2, :]
    else:
        selected_img = img
    plt.imshow(selected_img[:, :, ::-1] / np.max(img))
    plt.title('Selected')
    plt.show()
    return s.selected_rect


def select_vesicle_and_tether_in_path(path: Path) -> Generator[VesicleAndTetherImg, None, None]:
    """
    Display the images in the given path one by one and let the user choose the region of interest of the vesicle
    followed by choice of the tether region for each of the images.

    Usage:
    for vesicle, tether in select_vesicle_and_tether_in_path(work_path):
        # do stuff with vesicle and tether...

    Args:
        path: A path with png images

    Returns:
        A generator yielding the selected regions for vesicle and tether, the image, and the file's name.
    """
    for f in path.glob('*.png'):  # This looks for all files with .png in the path
        img = cv2.imread(str(f.absolute()), cv2.IMREAD_UNCHANGED) # str converts path to string and imread reads the file located in the path indicated by the string
        vesicle_rect = get_img_rect(img, 'Select vesicle and close')
        tether_rect = get_img_rect(img, 'Select tether and close')
        yield VesicleAndTetherImg(vesicle_rect, tether_rect, img, f.name)


def select_vesicle_and_tether_in_path_continuous(path: Path) -> Generator[VesicleAndTetherImg, None, None]:
    """
    Display the first image in the given path and let the user choose the region of interest of the vesicle
    followed by choice of the tether region. These regions will be used for all subsequent images in the path.
    This is useful in the case of multiple frames where there is little motion of the vesicle and tether areas.

    Usage:
    for vesicle, tether in select_vesicle_and_tether_in_path(work_path):
        # do stuff with vesicle and tether...

    Args:
        path: A path with png images

    Returns:
        A generator yielding the selected regions for vesicle and tether, the image, and the file's name.
    """
    for i, f in enumerate(path.glob('*.png')):  # This looks for all files with .png in the path
        img = cv2.imread(str(f.absolute()), cv2.IMREAD_UNCHANGED) # str converts path to string and imread reads the file located in the path indicated by the string
        if i == 0:
            vesicle_rect = get_img_rect(img, 'Select vesicle and close')
            tether_rect = get_img_rect(img, 'Select tether and close')
        yield VesicleAndTetherImg(vesicle_rect, tether_rect, img, f.name)
