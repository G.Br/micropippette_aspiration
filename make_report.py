from pathlib import Path
from typing import List
from dataclasses import dataclass
from csv import DictWriter

from matplotlib import pyplot as plt
import numpy as np
from cv2 import cv2

from pylake_utils import h5_to_png
from select_img_part import (
    Rect, VesicleAndTetherImg, select_vesicle_and_tether_in_path, select_vesicle_and_tether_in_path_continuous)
from tether_gpmv_photon_sum_otsus_guy import process_single_channel, ScanStats


@dataclass
class Row:
    name: str
    radius: int
    gpmv_stats: ScanStats
    tether_stats: ScanStats


def get_radius(img: np.ndarray, show_circle=False) -> float:
    img8bit = cv2.cvtColor(np.round(img / np.max(img) * 255).astype(np.uint8), cv2.COLOR_BGR2GRAY)
    found_circle = cv2.HoughCircles(
        img8bit, cv2.HOUGH_GRADIENT, 1, minDist=img.shape[0], param1=20,
        param2=20, minRadius=15, maxRadius=img.shape[0] // 2
    )
    if found_circle is None:
        return -1
    found_circle = found_circle[0, 0]
    if show_circle:
        res_img = cv2.circle(
            img8bit.copy(), tuple(np.round(found_circle[:2]).astype(int)), int(np.round(found_circle[2])), 255, 2
        )
        plt.figure()
        plt.imshow(res_img)
        plt.show()
    return found_circle[2]


def crop_img(img: np.ndarray, rect: Rect) -> np.ndarray:
    if rect:
        return img[rect.y1:rect.y2, rect.x1:rect.x2, :]
    return img


def image_stats(selected: VesicleAndTetherImg) -> Row:
    details = h5_to_png.parse_png_filename(selected.name)
    r = get_radius(selected.img) * details.pixel_size
    gpmv_img = crop_img(selected.img, selected.vesicle_rect)
    tether_img = crop_img(selected.img, selected.tether_rect)
    gpmv_stats = ScanStats(
        blue=process_single_channel(gpmv_img[:, :, 1]),
        green=process_single_channel(gpmv_img[:, :, 2])
    )
    tether_stats = ScanStats(
        blue=process_single_channel(tether_img[:, :, 1]),
        green=process_single_channel(tether_img[:, :, 2])
    )
    return Row(selected.name, round(r), gpmv_stats, tether_stats)


def make_report_single_shots(path: Path) -> List[Row]:
    return [image_stats(selected) for selected in select_vesicle_and_tether_in_path(path)]


def make_report_continuous(path: Path) -> List[Row]:
    return [image_stats(selected) for selected in select_vesicle_and_tether_in_path_continuous(path)]


def write_stats_csv(csv_file_name, rows: List[Row]):
    with open(csv_file_name, 'w', newline='') as csvfile:
        fieldnames = ['file', 'time stamps', 'sorting', 'gpmv radius',
                      'gpmv_photon_count_blue', 'gpmv_photon_count_green',
                      'tether_photon_count_blue', 'tether_photon_count_green',
                      ]
        writer = DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in rows:
            details = h5_to_png.parse_png_filename(row.name)
            timestamp = str(details.time)
            sorting = (
                        row.tether_stats.blue.density / row.tether_stats.green.density
                                                    ) / (
                        row.gpmv_stats.blue.density / row.gpmv_stats.green.density)
            writer.writerow({
                'file': row.name, 'time stamps': timestamp,
                'sorting': sorting, 'gpmv radius': row.radius,
                'gpmv_photon_count_blue': row.gpmv_stats.blue.photon_sum,
                'gpmv_photon_count_green': row.gpmv_stats.green.photon_sum,
                'tether_photon_count_blue': row.tether_stats.blue.photon_sum,
                'tether_photon_count_green': row.tether_stats.green.photon_sum,
            })


def process_path_single_shots(path: Path) -> None:
    rows = make_report_single_shots(path)
    write_stats_csv(path / 'results.csv', rows)


def process_path_single_continuous(path: Path) -> None:
    rows = make_report_continuous(path)
    write_stats_csv(path / 'results.csv', rows)


