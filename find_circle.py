import os
import sys
print(os.path.dirname(sys.executable))
import matplotlib.pyplot as plt
from lumicks import pylake  # <-- No pylake in colab, I'll pretend to have loaded green_image
import cv2
import numpy as np

file = pylake.File('F:\Raviv\\20210209\\20210209-115103 Scan 1.h5')
scan  = list(file.scans.values())[0]
img = np.round(cv2.normalize(scan.green_image, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)).astype(np.uint8)  # Normalize the image to be between 0 and 255 - This is the way openCV like it.

found_circle = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, minDist=img.shape[0], param1=20, param2=20, minRadius=15, maxRadius=img.shape[0] // 2)[0, 0]
res_img = cv2.circle(img, tuple(found_circle[:2]), int(np.round(found_circle[2])), 255, 2)

plt.figure()
plt.imshow(res_img)
plt.show()