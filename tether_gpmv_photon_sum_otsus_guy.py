import csv
from pathlib import Path
from dataclasses import dataclass

import numpy as np
from lumicks import pylake
from matplotlib import pyplot as plt


@dataclass
class ChannelStats:
    photon_sum: float
    density: float
    threshold: int


@dataclass
class ScanStats:
    blue: ChannelStats
    green: ChannelStats


PIXEL_SIZE = 0.086


def time_mod_1day(time_in_ns_from_epoch):
    secs_in_day = 24 * 3600
    return (time_in_ns_from_epoch * 1e-9) % secs_in_day


def get_scan(file):
    scan = file.scans[str(list(file.scans)[0])]
    return scan


# this function found the right threshold
def otsus_threshold(matrix):
    bins_num = np.max(matrix) + 1
    hist, bin_edges = np.histogram(matrix, bins=bins_num)
    # Calculate centers of bins
    bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
    # Iterate over all thresholds (indices) and get the probabilities w1(t), w2(t)
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # Get the class means mu0(t)
    mean1 = np.cumsum(hist * bin_mids) / weight1
    # Get the class means mu1(t)
    mean2 = (np.cumsum((hist * bin_mids)[::-1]) / weight2[::-1])[::-1]
    inter_class_variance = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2
    # Maximize the inter_class_variance function val
    index_of_max_val = np.argmax(inter_class_variance)
    threshold = bin_mids[:-1][index_of_max_val]
    print("Otsu's algorithm implementation thresholding result: ", threshold)

    return threshold


def process_single_channel(channel) -> ChannelStats:
    threshold = otsus_threshold(channel)
    channel[channel < threshold] = 0
    area = np.sum(channel > 0)
    photon_sum = np.sum(channel)
    return ChannelStats(photon_sum=photon_sum, density=photon_sum/area if area > 0 else 0, threshold=threshold)


def process_fluorescent_scan(scan, x1, y1, x2, y2) -> ScanStats:
    return ScanStats(
        blue=process_single_channel(scan.blue_image[y1:y2, x1:x2]),
        green=process_single_channel(scan.green_image[y1:y2, x1:x2])
    )


def write_stats_csv(csv_file_name, time, gpmv_stats, tether_stats):
    with open(csv_file_name, 'w', newline='') as csvfile:
        fieldnames = ['file', 'Tether_sum', 'GPMV_sum', 'timestamps', 'flurescence_density_GPMV', 'flurescence_density_tether',
                      'gpmv_Threshold', 'tether_Threshold', 'sorting']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for tether, gpmv in zip(tether_stats, gpmv_stats):
            writer.writerow({'file': file_name, 'Tether_sum': tether.photon_sum,
                             'GPMV_sum': gpmv.photon_sum, 'timestamps': time,
                             'flurescence_density_GPMV': gpmv.density,
                             'flurescence_density_tether': tether.density,
                             'gpmv_Threshold': gpmv.threshold, 'tether_Threshold': tether.threshold, 'sorting': 2})


def photon_count_tehter_GPMV(file):
    scan = get_scan(file)
    time = time_mod_1day(np.min(scan.timestamps))
    x1_gpmv, y1_gpmv, x2_gpmv, y2_gpmv = 25, 33, 100, 175
    x1_tether, y1_tether, x2_tether, y2_tether =188, 105, 336, 130
    x3_gpmv, y3_gpmv, x4_gpmv, y4_gpmv = 169, 107, 177, 118
    x3_tether, y3_tether, x4_tether, y4_tether = 178, 107, 186, 118
    gpmv_stats = process_fluorescent_scan(scan, x1=x1_gpmv, y1=y1_gpmv, x2=x2_gpmv, y2=y2_gpmv)
    tether_stats = process_fluorescent_scan(scan, x1=x1_tether, y1=y1_tether, x2=x2_tether, y2=y2_tether)
    write_stats_csv(
        f'blue_and_green_without_migrasome_otsus_tether_new_guy results for {file_directory.name}_GPMV_shorter_tether.csv',
        time=time, gpmv_stats=gpmv_stats, tether_stats=tether_stats
    )

    show_scan(scan, x1_gpmv, x1_tether, x2_gpmv, x2_tether, y1_gpmv, y1_tether, y2_gpmv, y2_tether,x3_gpmv, x3_tether, x4_gpmv, x4_tether, y3_gpmv, y3_tether, y4_gpmv, y4_tether)


def show_scan(scan, x1_gpmv, x1_tether, x2_gpmv, x2_tether, y1_gpmv, y1_tether, y2_gpmv, y2_tether,x3_gpmv, x3_tether, x4_gpmv, x4_tether, y3_gpmv, y3_tether, y4_gpmv, y4_tether):
    img = scan.rgb_image
    # here we switch the fluorecsnce chanels
    red_and_green = img[:, :, [1, 2, 0]]
    plt.figure()
    plt.xlabel(r'x [$\mu$m]')

    plt.ylabel(r'y [$\mu$m]')
    if y2_gpmv == int(scan.lines_per_frame) - 1:
        y2_gpmv = (scan.lines_per_frame)
    if y1_gpmv == 0:
        y1_gpmv = 1
    plt.imshow(red_and_green / np.max(red_and_green),
               extent=[0, img.shape[1] * PIXEL_SIZE, 0, img.shape[0] * PIXEL_SIZE])
    square_xs_tether = np.array([x1_tether, x2_tether, x2_tether, x1_tether, x1_tether]) * PIXEL_SIZE
    square_ys_tether = np.array([int(scan.lines_per_frame) - y1_tether, int(scan.lines_per_frame) - y1_tether,
                                 int(scan.lines_per_frame) - y2_tether, int(scan.lines_per_frame) - y2_tether,
                                 int(scan.lines_per_frame) - y1_tether]) * PIXEL_SIZE
    square_xs_GPMV = np.array([x1_gpmv, x2_gpmv, x2_gpmv, x1_gpmv, x1_gpmv]) * PIXEL_SIZE
    square_ys_GPMV = np.array(
        [int(scan.lines_per_frame) - y1_gpmv, int(scan.lines_per_frame) - y1_gpmv, int(scan.lines_per_frame) - y2_gpmv,
         int(scan.lines_per_frame) - y2_gpmv, int(scan.lines_per_frame) - y1_gpmv]) * PIXEL_SIZE
    square_xs_tether2 = np.array([x3_tether, x4_tether , x4_tether, x3_tether, x3_tether]) * PIXEL_SIZE
    square_ys_tether2 = np.array([int(scan.lines_per_frame) - y3_tether, int(scan.lines_per_frame) - y3_tether,
                                 int(scan.lines_per_frame) - y4_tether, int(scan.lines_per_frame) - y4_tether,
                                 int(scan.lines_per_frame) - y3_tether]) * PIXEL_SIZE
    square_xs_GPMV2 = np.array([x3_gpmv, x4_gpmv, x4_gpmv, x3_gpmv, x3_gpmv]) * PIXEL_SIZE
    square_ys_GPMV2 = np.array(
        [int(scan.lines_per_frame) - y3_gpmv, int(scan.lines_per_frame) - y3_gpmv, int(scan.lines_per_frame) - y4_gpmv,
         int(scan.lines_per_frame) - y4_gpmv, int(scan.lines_per_frame) - y3_gpmv]) * PIXEL_SIZE
    square_xs_GPMV3 = np.array([25, 100, 100, 25, 25]) * PIXEL_SIZE
    square_ys_GPMV3 = np.array(
        [int(scan.lines_per_frame) - 33, int(scan.lines_per_frame) - 33, int(scan.lines_per_frame) - 175,
         int(scan.lines_per_frame) - 175, int(scan.lines_per_frame) - 33]) * PIXEL_SIZE
    square_xs_GPMV4 = np.array([151, 159, 159, 151, 151]) * PIXEL_SIZE
    square_ys_GPMV4 = np.array(
        [int(scan.lines_per_frame) - 105, int(scan.lines_per_frame) - 105, int(scan.lines_per_frame) - 116,
         int(scan.lines_per_frame) - 116, int(scan.lines_per_frame) - 105]) * PIXEL_SIZE
    plt.plot(square_xs_GPMV, square_ys_GPMV)
    square_xs_GPMV5 = np.array([160, 168, 168, 160, 160]) * PIXEL_SIZE
    square_ys_GPMV5 = np.array(
        [int(scan.lines_per_frame) - 105, int(scan.lines_per_frame) - 105, int(scan.lines_per_frame) - 116,
         int(scan.lines_per_frame) - 116, int(scan.lines_per_frame) - 105]) * PIXEL_SIZE
    square_xs_GPMV6 = np.array([187, 195, 195, 187, 187]) * PIXEL_SIZE
    square_ys_GPMV6 = np.array(
        [int(scan.lines_per_frame) - 109, int(scan.lines_per_frame) - 109, int(scan.lines_per_frame) - 120,
         int(scan.lines_per_frame) - 120, int(scan.lines_per_frame) - 109]) * PIXEL_SIZE
    square_xs_GPMV7 = np.array([196, 204, 204, 196, 196]) * PIXEL_SIZE
    square_ys_GPMV7 = np.array(
        [int(scan.lines_per_frame) - 109, int(scan.lines_per_frame) - 109, int(scan.lines_per_frame) - 120,
         int(scan.lines_per_frame) - 120, int(scan.lines_per_frame) - 109]) * PIXEL_SIZE
    plt.plot(square_xs_GPMV, square_ys_GPMV)
    plt.plot(square_xs_tether, square_ys_tether)
    # plt.plot(square_xs_GPMV2, square_ys_GPMV2)
    # plt.plot(square_xs_tether2, square_ys_tether2)
    # plt.plot(square_xs_GPMV3, square_ys_GPMV3)
    # plt.plot(square_xs_GPMV4, square_ys_GPMV4)
    # plt.plot(square_xs_GPMV5, square_ys_GPMV5)
    # plt.plot(square_xs_GPMV6, square_ys_GPMV6)
    # plt.plot(square_xs_GPMV7, square_ys_GPMV7)
    plt.show()


if __name__ == '__main__':
    file_directory = Path(r'C:\Users\User\Desktop\Lab\TSPN\data_to_analyze\20210513\scans\20210513-182527 Scan 14.h5')
    file_name = r'C:\Users\User\Desktop\Lab\TSPN\data_to_analyze\20210513\scans\20210513-182527 Scan 14.h5'
    file = pylake.File(file_name)
    photon_count_tehter_GPMV(file)
